# Node.js API Starter

![Node.js CI build](https://github.com/aerabi/nodejs-api-starter/workflows/build/badge.svg)
![Node.js CI test](https://github.com/aerabi/nodejs-api-starter/workflows/tests/badge.svg)
![GitHub package.json version](https://img.shields.io/github/package-json/v/aerabi/nodejs-api-starter)
[![dependencies Status](https://status.david-dm.org/gh/aerabi/nodejs-api-starter.svg)](https://david-dm.org/aerabi/nodejs-api-starter)
[![devDependencies Status](https://status.david-dm.org/gh/aerabi/nodejs-api-starter.svg?type=dev)](https://david-dm.org/aerabi/nodejs-api-starter?type=dev)

A Node.js API starter project with TypeScript, TSOA, ESLint, Jest, and GitHub Actions.
